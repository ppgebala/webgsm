package pl.gsm.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;
import pl.gsm.dao.DBManager;
import pl.gsm.entity.Shop;

public class ShopConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Integer i = Integer.valueOf(value);
        EntityManager em = DBManager.getManager().createEntityManager();
        Shop p = em.find(Shop.class, i);
        em.close();
        return p;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Shop)) {
            throw new ConverterException(new FacesMessage("blad konwersji"));
        }
        Shop p = (Shop) value;
        return p.getId().toString();
    }

}
