package pl.gsm.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;
import pl.gsm.dao.DBManager;
import pl.gsm.entity.EmployeeUserType;

public class EmployeeUserTypeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Integer i = Integer.valueOf(value);
        EntityManager em = DBManager.getManager().createEntityManager();
        EmployeeUserType p = em.find(EmployeeUserType.class, i);
        em.close();
        return p;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof EmployeeUserType)) {
            throw new ConverterException(new FacesMessage("blad konwersji"));
        }
        EmployeeUserType p = (EmployeeUserType) value;
        return p.getId().toString();
    }

}
