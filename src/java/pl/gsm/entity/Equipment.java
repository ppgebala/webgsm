package pl.gsm.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "equipment")
@NamedQueries({
    @NamedQuery(name = "Equipment.findAll", query = "select e from Equipment e")})
public class Equipment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "box", nullable = false)
    private boolean box;
    @Column(name = "charger", nullable = false)
    private boolean charger;
    @Column(name = "manual", nullable = false)
    private boolean manual;
    @Column(name = "pccable", nullable = false)
    private boolean pccable;
    @Column(name = "memorycard", nullable = false)
    private boolean memorycard;
    @Column(name = "headset", nullable = false)
    private boolean headset;
    @Column(name = "warranty", nullable = false)
    private boolean warranty;
    @Column(name = "battery", nullable = false)
    private boolean battery;
    @OneToMany(mappedBy = "equipmentId", fetch = FetchType.EAGER)
    private Set<Phone> phoneSet;

    public Equipment() {
    }

    public Equipment(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Equipment)) {
            return false;
        }
        Equipment other = (Equipment) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Equipment[ id=" + id + " ]";
    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public boolean isCharger() {
        return charger;
    }

    public void setCharger(boolean charger) {
        this.charger = charger;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public boolean isPccable() {
        return pccable;
    }

    public void setPccable(boolean pccable) {
        this.pccable = pccable;
    }

    public boolean isMemorycard() {
        return memorycard;
    }

    public void setMemorycard(boolean memorycard) {
        this.memorycard = memorycard;
    }

    public boolean isHeadset() {
        return headset;
    }

    public void setHeadset(boolean headset) {
        this.headset = headset;
    }

    public boolean isWarranty() {
        return warranty;
    }

    public void setWarranty(boolean warranty) {
        this.warranty = warranty;
    }

    public boolean isBattery() {
        return battery;
    }

    public void setBattery(boolean battery) {
        this.battery = battery;
    }

    public Set<Phone> getPhoneSet() {
        return phoneSet;
    }

    public void setPhoneSet(Set<Phone> phoneSet) {
        this.phoneSet = phoneSet;
    }

}
