package pl.gsm.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "employeeUser")
@NamedQueries({
    @NamedQuery(name = "EmployeeUser.findAll", query = "select e from EmployeeUser e")})

public class EmployeeUser implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "userLogin", nullable = false, length = 30, unique = true)
    private String userLogin;
    @Column(name = "userPassword", nullable = false, length = 35)
    private String userPassword;
    @JoinColumn(name = "userType_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EmployeeUserType userTypeId;

    public EmployeeUser() {
    }

    public EmployeeUser(Integer id) {
        this.id = id;
    }

    public EmployeeUser(Integer id, String userLogin, String userPassword) {
        this.id = id;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = md5(userPassword);
    }

    public EmployeeUserType getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(EmployeeUserType userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof EmployeeUser)) {
            return false;
        }
        EmployeeUser other = (EmployeeUser) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    public static String md5(String input) {
        String md5 = null;
        if (null == input) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
        }
        return md5;
    }

}
