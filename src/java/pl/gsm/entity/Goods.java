package pl.gsm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "goods")
@NamedQueries({
    @NamedQuery(name = "Goods.findAll", query = "select e from Goods e")})
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "name", nullable = false, length = 40)
    private String name;
    @Column(name = "number", nullable = false)
    private Integer number;
    @JoinColumn(name = "taxId", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Tax taxId;
    @Column(name = "netPurchasePrice", nullable = false)
    private BigDecimal netPurchasePrice;
    @Column(name = "grossPurchasePrice", nullable = false)
    private BigDecimal grossPurchasePrice;
    @Column(name = "netSellingPrice", nullable = false)
    private BigDecimal netSellingPrice;
    @Column(name = "grossSellingPrice", nullable = false)
    private BigDecimal grossSellingPrice;
    @Column(name = "addedDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date addedDate = new Date();
    @Column(name = "modyficationDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modyficationDate = new Date();
    @JoinColumn(name = "typeGoodsId", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private TypeGoods typeGoodsId;
    @JoinColumn(name = "shopId", nullable = true, referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Shop shopId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Goods)) {
            return false;
        }
        Goods other = (Goods) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Goods[ id=" + id + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Tax getTaxId() {
        return taxId;
    }

    public void setTaxId(Tax taxId) {
        this.taxId = taxId;
    }

    public BigDecimal getNetPurchasePrice() {
        return netPurchasePrice;
    }

    public void setNetPurchasePrice(BigDecimal netPurchasePrice) {
        this.netPurchasePrice = netPurchasePrice.setScale(2);
        this.grossPurchasePrice = netPurchasePrice.add(
                (netPurchasePrice.multiply(new BigDecimal(
                                this.taxId.getRate()))).divide(new BigDecimal(100))).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getGrossPurchasePrice() {
        return grossPurchasePrice;
    }

    public void setGrossPurchasePrice(BigDecimal grossPurchasePrice) {
        this.netPurchasePrice = grossPurchasePrice.divide(
                BigDecimal.ONE.add(new BigDecimal(this.taxId.getRate()).divide(
                                new BigDecimal(100))), 2, RoundingMode.HALF_UP);
        this.grossPurchasePrice = grossPurchasePrice.setScale(2);
    }

    public BigDecimal getNetSellingPrice() {
        return netSellingPrice;
    }

    public void setNetSellingPrice(BigDecimal netSellingPrice) {
        this.netSellingPrice = netSellingPrice.setScale(2);
        this.grossSellingPrice = netSellingPrice.add(
                (netSellingPrice.multiply(new BigDecimal(
                                this.taxId.getRate()))).divide(new BigDecimal(100))).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getGrossSellingPrice() {
        return grossSellingPrice;
    }

    public void setGrossSellingPrice(BigDecimal grossSellingPrice) {
        this.netSellingPrice = grossSellingPrice.divide(
                BigDecimal.ONE.add(new BigDecimal(this.taxId.getRate()).divide(
                                new BigDecimal(100))), 2, RoundingMode.HALF_UP);
        this.grossSellingPrice = grossSellingPrice.setScale(2);
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getModyficationDate() {
        return modyficationDate;
    }

    public void setModyficationDate(Date modyficationDate) {
        this.modyficationDate = modyficationDate;

    }

    public TypeGoods getTypeGoodsId() {
        return typeGoodsId;
    }

    public void setTypeGoodsId(TypeGoods typeGoodsId) {
        this.typeGoodsId = typeGoodsId;
    }

    public Shop getShopId() {
        return shopId;
    }

    public void setShopId(Shop shopId) {
        this.shopId = shopId;
    }

}
