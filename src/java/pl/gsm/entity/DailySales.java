package pl.gsm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "dailysales")
@NamedQueries({
    @NamedQuery(name = "DailySales.findAll", query = "select e from DailySales e")})
public class DailySales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "sellingDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sellingDate = new Date();
    @JoinColumn(name = "employeeUserId", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EmployeeUser employeeUserId;
    @Column(name = "category", nullable = false, length = 40)
    private String category;
    @Column(name = "product", nullable = false, length = 50)
    private String product;
    @Column(name = "number", nullable = false)
    private Integer number;
    @Column(name = "unitPrice", nullable = false)
    private BigDecimal unitPrice;
    @Column(name = "sumPrice", nullable = false)
    private BigDecimal sumPrice;
    @JoinColumn(name = "shopId", nullable = true, referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Shop shopId;

    public DailySales() {
    }

    public DailySales(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof DailySales)) {
            return false;
        }
        DailySales other = (DailySales) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Product[ id=" + id + " ]";
    }

    public Date getSellingDate() {
        return sellingDate;
    }

    public void setSellingDate(Date sellingDate) {
        this.sellingDate = sellingDate;
    }

    public EmployeeUser getEmployeeUserId() {
        return employeeUserId;
    }

    public void setEmployeeUserId(EmployeeUser employeeUserId) {
        this.employeeUserId = employeeUserId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        this.sumPrice = unitPrice.multiply(new BigDecimal(number));
    }

    public BigDecimal getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(BigDecimal sumPrice) {
        this.sumPrice = sumPrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Shop getShopId() {
        return shopId;
    }

    public void setShopId(Shop shopId) {
        this.shopId = shopId;
    }

}
