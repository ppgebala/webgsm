package pl.gsm.entity;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employeeUserType")
@NamedQueries({
    @NamedQuery(name = "EmployeeUserType.findAll", query = "select e from EmployeeUserType e")})
public class EmployeeUserType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "userType", nullable = false, length = 10, unique = true)
    private String type;
    @OneToMany(mappedBy = "userTypeId", fetch = FetchType.EAGER)
    private Set<EmployeeUser> employeeSet;

    public EmployeeUserType() {
    }

    public EmployeeUserType(Integer id) {
        this.id = id;
    }

    public EmployeeUserType(Integer id, String userType) {
        this.id = id;
        this.type = userType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<EmployeeUser> getEmployeeSet() {
        return employeeSet;
    }

    public void setEmployeeSet(Set<EmployeeUser> employeeSet) {
        this.employeeSet = employeeSet;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof EmployeeUserType)) {
            return false;
        }
        EmployeeUserType other = (EmployeeUserType) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

}
