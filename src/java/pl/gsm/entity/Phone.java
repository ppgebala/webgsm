package pl.gsm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "phone")
@NamedQueries({
    @NamedQuery(name = "Phone.findAll", query = "select e from Phone e")})
public class Phone implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "producer", nullable = false, length = 20)
    private String producer;
    @Column(name = "model", nullable = false, length = 30)
    private String model;
    @Column(name = "imeiNumber", nullable = false, length = 15, unique = true)
    private long imeiNumber;
    @JoinColumn(name = "sellerId", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Seller sellerId;
    @JoinColumn(name = "equipmentId", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Equipment equipmentId;
    @Column(name = "purchaseDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date purchaseDate = new Date();
    @Column(name = "saleDate", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date saleDate;
    @Column(name = "netPurchasePrice", nullable = false)
    private BigDecimal netPurchasePrice;
    @Column(name = "grossPurchasePrice", nullable = false)
    private BigDecimal grossPurchasePrice;
    @Column(name = "netSellingPrice", nullable = false)
    private BigDecimal netSellingPrice;
    @Column(name = "grossSellingPrice", nullable = false)
    private BigDecimal grossSellingPrice;
    @Column(name = "statePhone", nullable = false, length = 30)
    private String statePhone;
    @Column(name = "simlock", nullable = false)
    private boolean simlock;
    @Column(name = "guarantees", nullable = false, length = 100)
    private String guarantees;
    @Column(name = "description", nullable = true, length = 100)
    private String description;
    @JoinColumn(name = "taxId", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Tax taxId;
    @JoinColumn(name = "shopId", nullable = true, referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Shop shopId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Phone)) {
            return false;
        }
        Phone other = (Phone) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Phone[ id=" + id + " ]";
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public long getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(long imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public Seller getSellerId() {
        return sellerId;
    }

    public void setSellerId(Seller sellerId) {
        this.sellerId = sellerId;
    }

    public Equipment getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Equipment equipmentId) {
        this.equipmentId = equipmentId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public BigDecimal getNetPurchasePrice() {
        return netPurchasePrice;
    }

    public void setNetPurchasePrice(BigDecimal netPurchasePrice) {
        this.netPurchasePrice = netPurchasePrice.setScale(2);
        this.grossPurchasePrice = netPurchasePrice.add(
                (netPurchasePrice.multiply(new BigDecimal(
                                this.taxId.getRate()))).divide(new BigDecimal(100))).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getGrossPurchasePrice() {
        return grossPurchasePrice;
    }

    public void setGrossPurchasePrice(BigDecimal grossPurchasePrice) {
        this.netPurchasePrice = grossPurchasePrice.divide(
                BigDecimal.ONE.add(new BigDecimal(this.taxId.getRate()).divide(
                                new BigDecimal(100))), 2, RoundingMode.HALF_UP);
        this.grossPurchasePrice = grossPurchasePrice.setScale(2);
    }

    public BigDecimal getNetSellingPrice() {
        return netSellingPrice;
    }

    public void setNetSellingPrice(BigDecimal netSellingPrice) {
        this.netSellingPrice = netSellingPrice.setScale(2);
        this.grossSellingPrice = netSellingPrice.add(
                (netSellingPrice.multiply(new BigDecimal(
                                this.taxId.getRate()))).divide(new BigDecimal(100))).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getGrossSellingPrice() {
        return grossSellingPrice;
    }

    public void setGrossSellingPrice(BigDecimal grossSellingPrice) {
        this.netSellingPrice = grossSellingPrice.divide(
                BigDecimal.ONE.add(new BigDecimal(this.taxId.getRate()).divide(
                                new BigDecimal(100))), 2, RoundingMode.HALF_UP);
        this.grossSellingPrice = grossSellingPrice.setScale(2);
    }

    public String getStatePhone() {
        return statePhone;
    }

    public void setStatePhone(String statePhone) {
        this.statePhone = statePhone;
    }

    public boolean isSimlock() {
        return simlock;
    }

    public void setSimlock(boolean simlock) {
        this.simlock = simlock;
    }

    public String getGuarantees() {
        return guarantees;
    }

    public void setGuarantees(String guarantees) {
        this.guarantees = guarantees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Tax getTaxId() {
        return taxId;
    }

    public void setTaxId(Tax taxId) {
        this.taxId = taxId;
    }

    public Shop getShopId() {
        return shopId;
    }

    public void setShopId(Shop shopId) {
        this.shopId = shopId;
    }

}
