package pl.gsm.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "seller")
@NamedQueries({
    @NamedQuery(name = "Seller.findAll", query = "select e from Seller e")})
public class Seller implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "firstName", nullable = false, length = 20)
    private String firstName;
    @Column(name = "lastName", nullable = false, length = 20)
    private String lastName;
    @Column(name = "companyName", nullable = true, length = 40)
    private String companyName;
    @JoinColumn(name = "adressId", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Adress adressId;
    @Column(name = "phoneNumber", nullable = true, length = 11)
    private long phoneNumber;
    @Column(name = "emailAdress", nullable = true, length = 50)
    private String emailAdress;
    @Column(name = "nipNumber", nullable = true, length = 10, unique = true)
    private long nipNumber;
    @Column(name = "documentType", nullable = true, length = 20)
    private String documentType;
    @Column(name = "documentNumber", nullable = true, length = 20, unique = true)
    private String documentNumber;

    public Seller() {
    }

    public Seller(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Seller)) {
            return false;
        }
        Seller other = (Seller) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Seller[ id=" + id + " ]";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Adress getAdressId() {
        return adressId;
    }

    public void setAdressId(Adress adressId) {
        this.adressId = adressId;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public long getNipNumber() {
        return nipNumber;
    }

    public void setNipNumber(long nipNumber) {
        this.nipNumber = nipNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

}
