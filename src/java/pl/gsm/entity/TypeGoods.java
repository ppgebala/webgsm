
package pl.gsm.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "typeGoods")
@NamedQueries({
    @NamedQuery(name = "TypeGoods.findAll", query = "select e from TypeGoods e")})
public class TypeGoods implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "categoryName", nullable = false, length = 30)
    private String categoryName;
    
    @OneToMany(mappedBy = "typeGoodsId", fetch = FetchType.EAGER)
    private Set<Goods> goodsSet;

    public TypeGoods() {
    }

    public TypeGoods(Integer id) {
        this.id = id;
    }

    public TypeGoods(Integer id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TypeGoods)) {
            return false;
        }
        TypeGoods other = (TypeGoods) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.typeGoods[ id=" + id + " ]";
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<Goods> getGoodsSet() {
        return goodsSet;
    }

    public void setGoodsSet(Set<Goods> goodsSet) {
        this.goodsSet = goodsSet;
    }
    
}
