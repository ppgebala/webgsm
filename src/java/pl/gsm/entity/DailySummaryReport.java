package pl.gsm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "dailySummaryReport")
@NamedQueries({
    @NamedQuery(name = "DailySummaryReport.findAll", query = "select e from DailySummaryReport e")})
public class DailySummaryReport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "moneyEarned", nullable = false)
    private BigDecimal moneyEarned;
    @Column(name = "moneySpent", nullable = false)
    private BigDecimal moneySpent;
    @Column(name = "moneySum", nullable = false)
    private BigDecimal moneySum;
    @Column(name = "moneyReport", nullable = false)
    private BigDecimal moneyReport;
    @Column(name = "totalStateCash", nullable = false)
    private BigDecimal totalStateCash;
    @Column(name = "summaryDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date summaryDate = new Date();

    public DailySummaryReport() {
    }

    public DailySummaryReport(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof DailySummaryReport)) {
            return false;
        }
        DailySummaryReport other = (DailySummaryReport) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.DailySummaryReport[ id=" + id + " ]";
    }

    public BigDecimal getMoneyEarned() {
        return moneyEarned;
    }

    public void setMoneyEarned(BigDecimal moneyEarned) {
        this.moneyEarned = moneyEarned;
    }

    public BigDecimal getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(BigDecimal moneySpent) {
        this.moneySpent = moneySpent;
    }

    public BigDecimal getMoneySum() {
        return moneySum;
    }

    public void setMoneySum(BigDecimal moneySum) {
        this.moneySum = moneySum;
    }

    public BigDecimal getMoneyReport() {
        return moneyReport;
    }

    public void setMoneyReport(BigDecimal moneyReport) {
        this.moneyReport = moneyReport;
    }

    public BigDecimal getTotalStateCash() {
        return totalStateCash;
    }

    public void setTotalStateCash(BigDecimal totalStateCash) {
        this.totalStateCash = totalStateCash;
    }

    public Date getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(Date summaryDate) {
        this.summaryDate = summaryDate;
    }

}
