package pl.gsm.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shop")
@NamedQueries({
    @NamedQuery(name = "Shop.findAll", query = "select e from Shop e")})
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "name", nullable = false, length = 20)
    private String name;
    @OneToMany(mappedBy = "shopId", fetch = FetchType.EAGER)
    private Set<DailySales> dailySalesSet;
    @OneToMany(mappedBy = "shopId", fetch = FetchType.EAGER)
    private Set<Goods> goodsSet;
    @OneToMany(mappedBy = "shopId", fetch = FetchType.EAGER)
    private Set<Phone> phoneSet;
    @OneToMany(mappedBy = "shopId", fetch = FetchType.EAGER)
    private Set<Service> serviceSet;

    public Shop() {
    }

    public Shop(Integer id) {
        this.id = id;
    }

    public Shop(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Tax)) {
            return false;
        }
        Shop other = (Shop) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "pl.gsm.entity.Shop[ id=" + id + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DailySales> getDailySalesSet() {
        return dailySalesSet;
    }

    public void setDailySalesSet(Set<DailySales> dailySalesSet) {
        this.dailySalesSet = dailySalesSet;
    }

    public Set<Goods> getGoodsSet() {
        return goodsSet;
    }

    public void setGoodsSet(Set<Goods> goodsSet) {
        this.goodsSet = goodsSet;
    }

    public Set<Phone> getPhoneSet() {
        return phoneSet;
    }

    public void setPhoneSet(Set<Phone> phoneSet) {
        this.phoneSet = phoneSet;
    }

    public Set<Service> getServiceSet() {
        return serviceSet;
    }

    public void setServiceSet(Set<Service> serviceSet) {
        this.serviceSet = serviceSet;
    }

}
