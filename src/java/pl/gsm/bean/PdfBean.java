package pl.gsm.bean;

import pl.gsm.dao.DailySalesBean;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import pl.gsm.dao.DBManager;
import pl.gsm.entity.DailySales;

@ManagedBean(name = "pdfBean")
@RequestScoped
public class PdfBean {

    private final BaseFont[] baseFontVerdana = new BaseFont[1];
    private Document document;
    Font font6, font8, font8b, font10, font10b, font14;
    public static String RESULT = "C:/Users/Piotrua/Desktop/webGSM/pdf/fv.pdf";
    private String companyData, dealerName, dealerAdress, dealerPhone, dealerNIP,
            buyerName, buyerAdress, buyerPhone, buyerNIP, cashPaid, paymentWords,
            placeOfIssue, title;

    private String pkwiu, measure, path = "", filename = "";
    private int discount, tax;
    private BigDecimal vatValueSum, netValueSum, grossValueSum;

    private Date dateOfIssue, dateOfSale;

    private static List<DailySales> dailySalesToFvList;
    private Map<Integer, Boolean> checked = new HashMap<>();

    public PdfBean() throws DocumentException, IOException {
        initFonts();
        initValue();

    }

    public String loadCheckedDailySales() {
        EntityManager em;
        DailySalesBean dsb = new DailySalesBean();
        List<DailySales> dailySalesList = dsb.getDailySalesList();
        dailySalesToFvList = new ArrayList();
        DailySales dailySales;
        for (DailySales entity : dailySalesList) {
            if (getChecked().get(entity.getId())) {
                em = DBManager.getManager().createEntityManager();
                em.getTransaction().begin();
                dailySales = em.find(DailySales.class, entity.getId());
                getDailySalesToFvList().add(dailySales);
                em.getTransaction().commit();
                em.close();
            }
        }
        return "/generatef.xhtml";
    }

    private void initFonts() throws DocumentException, IOException {
        baseFontVerdana[0] = BaseFont.createFont("c:/Users/Piotrua/Desktop/webGSM/pdf/verdana.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        font6 = new Font(baseFontVerdana[0], 6, Font.NORMAL);
        font8 = new Font(baseFontVerdana[0], 8, Font.NORMAL);
        font8b = new Font(baseFontVerdana[0], 8, Font.BOLD);
        font10 = new Font(baseFontVerdana[0], 10, Font.NORMAL);
        font10b = new Font(baseFontVerdana[0], 10, Font.BOLD);
        font14 = new Font(baseFontVerdana[0], 14, Font.NORMAL);
    }

    private void initValue() {
        companyData = "Piotr Gębala F.H.U. WebGSM\n"
                + "Kapelanka 6a, 30-347 Kraków\n"
                + "Tel. 505 505 505, NIP 987-654-32-10\n"
                + "Millenium Bank,\n"
                + "00 0000 0000 0000 0000 0000 0000";

        dealerName = "Piotr Gębala F.H.U. WebGSM";
        dealerAdress = "Kapelanka 6a, 30-347 Kraków";
        dealerPhone = "Tel. 505 505 505";
        dealerNIP = "NIP 987-654-32-10";

        buyerName = "Piotr Gębala F.H.U. WebGSM";
        buyerAdress = "Kapelanka 6a, 30-347 Kraków";
        buyerPhone = "Tel. 505 505 505";
        buyerNIP = "NIP 987-654-32-10";

        cashPaid = "0";
        paymentWords = "zero PLN 0/100";
        placeOfIssue = "Kraków";
        dateOfIssue = new Date();
        dateOfSale = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateOfIssue);
        title = cal.get(Calendar.DAY_OF_MONTH) + "/" + placeOfIssue.substring(0, 3).toUpperCase() + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);

        pkwiu = " ";
        measure = "szt.";
        setDiscount(0);
        setTax(23);
    }

    public void downloadPDF() throws IOException {

        FacesContext fC = FacesContext.getCurrentInstance();
        ExternalContext eC = fC.getExternalContext();
        HttpServletResponse res = (HttpServletResponse) eC.getResponse();

        File file = new File(path, filename);
        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new FileInputStream(file), 10240);

            res.reset();
            res.setHeader("Content-Type", "application/pdf");
            res.setHeader("Content-Length", String.valueOf(file.length()));
            res.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
            out = new BufferedOutputStream(res.getOutputStream(), 10240);

            byte[] buffer = new byte[10240];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            out.flush();
        } finally {
            close(out);
            close(in);
        }

        fC.responseComplete();
    }

    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
            }
        }
    }

    public void createPdf() throws DocumentException, FileNotFoundException, IOException {
        path = "c:/Users/Piotrua/Desktop/webGSM/pdf/";
        filename = "Fv" + String.valueOf(new Date()).replace(":", "").replace(" ", "") + ".pdf";
        RESULT = path + filename;
        setDocument(new Document());
        PdfWriter.getInstance(getDocument(), new FileOutputStream(RESULT));
        getDocument().open();
        getDocument().setMargins(10, 10, 10, 10);
        getDocument().add(createMainTable());
        getDocument().close();
        downloadPDF();
    }

    public void calculate() {
        BigDecimal netPrice, netVal, vatVal;
        setNetValueSum(new BigDecimal(BigInteger.ZERO));
        setVatValueSum(new BigDecimal(BigInteger.ZERO));
        setGrossValueSum(new BigDecimal(BigInteger.ZERO));

        for (DailySales dsl : dailySalesToFvList) {
            netPrice = dsl.getUnitPrice().divide(
                    BigDecimal.ONE.add(new BigDecimal(tax).divide(
                                    new BigDecimal(100))), 2, RoundingMode.HALF_UP);
            netVal = netPrice.multiply(new BigDecimal(dsl.getNumber()));
            vatVal = netPrice.multiply(new BigDecimal(tax).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
            setNetValueSum(getNetValueSum().add(netVal));
            setVatValueSum(getVatValueSum().add(vatVal));
            setGrossValueSum(getGrossValueSum().add(dsl.getSumPrice()));
        }
    }

    private String convertDate(Date date) {
        String formatDate;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        formatDate = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);
        return formatDate;
    }

    private PdfPTable createSummaryTable() {
        PdfPTable tableSummary = new PdfPTable(4);
        PdfPCell cellSummary = new PdfPCell();
        cellSummary.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellSummary.setPhrase(new Phrase("stawka VAT", font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase("wartość netto", font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase("kwota VAT", font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase("wartość brutto", font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setBackgroundColor(BaseColor.WHITE);
        cellSummary.setPhrase(new Phrase(String.valueOf(tax), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(netValueSum), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(vatValueSum), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(grossValueSum), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setBorder(0);
        cellSummary.setPhrase(new Phrase("Razem:", font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(netValueSum), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(vatValueSum), font8));
        tableSummary.addCell(cellSummary);
        cellSummary.setPhrase(new Phrase(String.valueOf(grossValueSum), font8));
        tableSummary.addCell(cellSummary);
        return tableSummary;
    }

    private PdfPTable createMiniTable() {
        PdfPTable tableMini = new PdfPTable(2);
        PdfPCell cellMini = new PdfPCell();
        cellMini.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellMini.setBorderWidth(0);
        cellMini.setBorderWidthTop(1);
        cellMini.setPhrase(new Phrase("Razem do zapłaty: ", font10b));
        tableMini.addCell(cellMini);
        cellMini.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellMini.setPhrase(new Phrase(String.valueOf(grossValueSum), font10b));
        tableMini.addCell(cellMini);
        cellMini.setBorderWidthTop(0);
        cellMini.setBorderWidthBottom(1);
        cellMini.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellMini.setBackgroundColor(BaseColor.WHITE);
        cellMini.setPhrase(new Phrase("Słownie: ", font8b));
        tableMini.addCell(cellMini);
        cellMini.setPhrase(new Phrase(getPaymentWords(), font8));
        cellMini.setPaddingBottom(10);
        tableMini.addCell(cellMini);
        return tableMini;
    }

    private PdfPTable createSignatureTable(String a, String b) {
        PdfPTable tableSignature = new PdfPTable(1);
        PdfPCell cellRight = new PdfPCell();
        cellRight.setBorder(0);
        cellRight.setBorderWidthLeft(1);
        cellRight.setBorderWidthRight(1);
        cellRight.setBorderWidthTop(1);
        cellRight.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellRight.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellRight.setPhrase(new Phrase(a, font10b));
        tableSignature.addCell(cellRight);
        cellRight.setBorderWidthTop(0);
        cellRight.setBackgroundColor(BaseColor.WHITE);
        cellRight.setPhrase(new Phrase(" "));
        tableSignature.addCell(cellRight);
        tableSignature.addCell(cellRight);
        tableSignature.addCell(cellRight);
        cellRight.setBorderWidthBottom(1);
        tableSignature.addCell(cellRight);
        cellRight.setBorder(0);
        cellRight.setPhrase(new Phrase("Podpis osoby upoważnionej do " + b + " faktury VAT", font6));
        tableSignature.addCell(cellRight);
        return tableSignature;
    }

    private PdfPTable createGoodsTable() {
        PdfPTable tableGoods = new PdfPTable(26);
        PdfPCell cellGoods = new PdfPCell(new Phrase("Lp.", font8));
        cellGoods.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellGoods.setColspan(1);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Nazwa", font8));
        cellGoods.setColspan(6);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("PKWiU", font8));
        cellGoods.setColspan(2);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Ilość", font8));
        cellGoods.setColspan(2);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("j.m.", font8));
        cellGoods.setColspan(1);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Rabat[%]", font8));
        cellGoods.setColspan(2);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Cena netto", font8));
        cellGoods.setColspan(3);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("VAT[%]", font8));
        cellGoods.setColspan(1);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Wartość netto", font8));
        cellGoods.setColspan(3);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("VAT", font8));
        cellGoods.setColspan(2);
        tableGoods.addCell(cellGoods);
        cellGoods.setPhrase(new Phrase("Wartość brutto", font8));
        cellGoods.setColspan(3);
        tableGoods.addCell(cellGoods);

        cellGoods.setBackgroundColor(BaseColor.WHITE);
        BigDecimal netPrice, netVal, vatVal;
        for (int i = 0; i < dailySalesToFvList.size(); i++) {
            netPrice = dailySalesToFvList.get(i).getUnitPrice().divide(
                    BigDecimal.ONE.add(new BigDecimal(tax).divide(
                                    new BigDecimal(100))), 2, RoundingMode.HALF_UP);
            netVal = netPrice.multiply(new BigDecimal(dailySalesToFvList.get(i).getNumber()));
            vatVal = netPrice.multiply(new BigDecimal(tax).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
            cellGoods.setColspan(1);
            cellGoods.setPhrase(new Phrase(String.valueOf(i + 1), font8));
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(dailySalesToFvList.get(i).getCategory()
                    + " " + dailySalesToFvList.get(i).getProduct(), font8));
            cellGoods.setColspan(6);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(pkwiu, font8));
            cellGoods.setColspan(2);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(dailySalesToFvList.get(i).getNumber()), font8));
            cellGoods.setColspan(2);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(measure, font8));
            cellGoods.setColspan(1);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(getDiscount()), font8));
            cellGoods.setColspan(2);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(netPrice), font8));
            cellGoods.setColspan(3);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(getTax()), font8));
            cellGoods.setColspan(1);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(netVal), font8));
            cellGoods.setColspan(3);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(vatVal), font8));
            cellGoods.setColspan(2);
            tableGoods.addCell(cellGoods);
            cellGoods.setPhrase(new Phrase(String.valueOf(dailySalesToFvList.get(i).getSumPrice()), font8));
            cellGoods.setColspan(3);
            tableGoods.addCell(cellGoods);
        }
        return tableGoods;
    }

    private PdfPTable createMainTable() {
        PdfPTable table = new PdfPTable(2);
        PdfPCell cell;
        table.setWidthPercentage(100);
        cell = new PdfPCell(new Phrase(getCompanyData(), font10));
        cell.setRowspan(6);
        cell.setBorder(0);
        table.addCell(cell);

        cell.setMinimumHeight(18);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(1);
        cell.setPhrase(new Phrase("Miejsce wystawienia:", font10));
        cell.setBorderWidthTop(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);
        cell.setPhrase(new Phrase(getPlaceOfIssue(), font10));
        cell.setBorderWidthTop(0);
        cell.setBackgroundColor(BaseColor.WHITE);
        table.addCell(cell);
        cell.setPhrase(new Phrase("Data sprzedaży:", font10));
        cell.setBorderWidthTop(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);
        cell.setPhrase(new Phrase(convertDate(getDateOfSale()), font10));
        cell.setBorderWidthTop(0);
        cell.setBackgroundColor(BaseColor.WHITE);
        table.addCell(cell);
        cell.setPhrase(new Phrase("Data wystawienia:", font10));
        cell.setBorderWidthTop(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);
        cell.setPhrase(new Phrase(convertDate(getDateOfIssue()), font10));
        cell.setBorderWidthTop(0);
        cell.setBackgroundColor(BaseColor.WHITE);
        cell.setPaddingBottom(30);
        table.addCell(cell);

        cell.setPhrase(new Phrase("Sprzedawca:", font10));
        cell.setPaddingBottom(10);
        cell.setBorderWidthTop(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);
        cell.setPhrase(new Phrase("Nabywca:", font10));
        table.addCell(cell);

        cell.setBorderWidthTop(0);
        cell.setBackgroundColor(BaseColor.WHITE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setPhrase(new Phrase(getDealerName(), font10));
        table.addCell(cell);
        cell.setPhrase(new Phrase(getBuyerName(), font10));
        table.addCell(cell);
        cell.setBorderWidthBottom(1);
        cell.setPhrase(new Phrase(getDealerAdress() + "\n" + getDealerPhone() + "\n" + getDealerNIP(), font10));
        table.addCell(cell);
        cell.setPhrase(new Phrase(getBuyerAdress() + "\n" + getBuyerPhone() + "\n" + getBuyerNIP(), font10));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Faktura VAT " + title, font14));
        cell.setBorder(0);
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
        cell.setColspan(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(createGoodsTable());
        cell.setColspan(2);
        cell.setRowspan(10);
        cell.setBorder(0);
        table.addCell(cell);

        cell.setPhrase(new Phrase(" "));
        cell.setColspan(1);
        cell.setRowspan(1);
        cell.setBorder(0);

        table.addCell(cell);
        cell = new PdfPCell(createSummaryTable());
        cell.setPaddingTop(10);
        cell.setColspan(1);
        cell.setBorder(0);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Zapłacono gotówką: " + getCashPaid(), font10b));
        cell.setBorder(0);
        cell.setPaddingTop(5);
        cell.setRowspan(1);
        table.addCell(cell);

        cell = new PdfPCell(createMiniTable());
        cell.setRowspan(1);
        cell.setBorderWidth(0);
        cell.setPaddingTop(5);
        cell.setPaddingBottom(20);
        table.addCell(cell);

        cell = new PdfPCell(createSignatureTable("Odebrał(a)", "odebrania"));
        cell.setPaddingRight(20);
        cell.setBorder(0);
        table.addCell(cell);

        cell = new PdfPCell(createSignatureTable("Wystawił(a)", "wystawienia"));
        cell.setPaddingLeft(20);
        cell.setBorder(0);
        table.addCell(cell);
        return table;
    }

    public String getCompanyData() {
        return companyData;
    }

    public void setCompanyData(String companyData) {
        this.companyData = companyData;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerAdress() {
        return dealerAdress;
    }

    public void setDealerAdress(String dealerAdress) {
        this.dealerAdress = dealerAdress;
    }

    public String getDealerPhone() {
        return dealerPhone;
    }

    public void setDealerPhone(String dealerPhone) {
        this.dealerPhone = dealerPhone;
    }

    public String getDealerNIP() {
        return dealerNIP;
    }

    public void setDealerNIP(String dealerNIP) {
        this.dealerNIP = dealerNIP;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerAdress() {
        return buyerAdress;
    }

    public void setBuyerAdress(String buyerAdress) {
        this.buyerAdress = buyerAdress;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public String getBuyerNIP() {
        return buyerNIP;
    }

    public void setBuyerNIP(String buyerNIP) {
        this.buyerNIP = buyerNIP;
    }

    public String getCashPaid() {
        return cashPaid;
    }

    public void setCashPaid(String cashPaid) {
        this.cashPaid = cashPaid;
    }

    public String getPaymentWords() {
        return paymentWords;
    }

    public void setPaymentWords(String paymentWords) {
        this.paymentWords = paymentWords;
    }

    public String getPlaceOfIssue() {
        return placeOfIssue;
    }

    public void setPlaceOfIssue(String placeOfIssue) {
        this.placeOfIssue = placeOfIssue;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(Date dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateOfIssue);
        this.title = cal.get(Calendar.DAY_OF_MONTH) + "/" + placeOfIssue.substring(0, 3).toUpperCase() + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);
    }

    public List<DailySales> getDailySalesToFvList() {
        return dailySalesToFvList;
    }

    public void setDailySalesToFvList(List<DailySales> dailySalesToFvList) {
        this.dailySalesToFvList = dailySalesToFvList;
    }

    public Map<Integer, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Integer, Boolean> checked) {
        this.checked = checked;
    }

    public String getPkwiu() {
        return pkwiu;
    }

    public void setPkwiu(String pkwiu) {
        this.pkwiu = pkwiu;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public BigDecimal getVatValueSum() {
        return vatValueSum;
    }

    public void setVatValueSum(BigDecimal vatValueSum) {
        this.vatValueSum = vatValueSum;
    }

    public BigDecimal getNetValueSum() {
        return netValueSum;
    }

    public void setNetValueSum(BigDecimal netValueSum) {
        this.netValueSum = netValueSum;
    }

    public BigDecimal getGrossValueSum() {
        return grossValueSum;
    }

    public void setGrossValueSum(BigDecimal grossValueSum) {
        this.grossValueSum = grossValueSum;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

}
