package pl.gsm.bean;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import pl.gsm.dao.DBManager;

public class EmployeeContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DBManager.getManager().closeEntityManagerFactory();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DBManager.getManager().createEntityManagerFactory();
    }

}
