package pl.gsm.bean;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpSession;
import pl.gsm.dao.DBManager;
import pl.gsm.dao.EmployeeUserBean;
import pl.gsm.entity.EmployeeUser;
import pl.gsm.entity.Shop;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userPassword;
    private String userLogin;
    private Integer shopId;
    private static Shop shop;
    private static EmployeeUser user;

    private HttpSession getSession() {
        return (HttpSession) FacesContext.
                getCurrentInstance().
                getExternalContext().
                getSession(false);
    }

    public boolean hasRole() {
        return user.getUserTypeId().getType().equals("szef");
    }

    public String login() {
        if (EmployeeUserBean.checkLoginAndPassword(userLogin, userPassword)) {

            HttpSession httpSession = getSession();
            httpSession.setAttribute("username", userLogin);
            assignUser();
            assignShop();
            return "adddailysales";
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Błędne logowanie!",
                            "Spróbuj jeszcze raz!"));
            return "login";
        }
    }

    public String logout() {
        HttpSession httpSession = getSession();
        httpSession.invalidate();
        return "login";
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = EmployeeUser.md5(userPassword);
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public static Shop getShop() {
        return shop;
    }

    public static void setShop(Shop aSklep2) {
        shop = aSklep2;
    }

    public static EmployeeUser getUser() {
        return user;
    }

    public static void setUser(EmployeeUser aUser) {
        user = aUser;
    }

    public void assignShop() {
        EntityManager em = DBManager.getManager().createEntityManager();
        TypedQuery<Shop> query = em.createQuery("select e from Shop e where e.id = :id", Shop.class);
        query.setParameter("id", shopId);
        shop = query.getSingleResult();
        em.close();
    }

    public void assignUser() {
        EntityManager em = DBManager.getManager().createEntityManager();
        TypedQuery<EmployeeUser> query = em.createQuery("select e from EmployeeUser e where e.userLogin = :userLogin", EmployeeUser.class);
        query.setParameter("userLogin", userLogin);
        user = query.getSingleResult();
        em.close();
    }
}
