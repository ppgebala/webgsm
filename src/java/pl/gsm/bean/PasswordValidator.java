package pl.gsm.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("PasswordValidator")
public class PasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (!isPassword(value.toString())) {
            FacesMessage msg = new FacesMessage("PASSWORD validation failed", "Invalid PASSWORD");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

    private boolean isPassword(String password) {
        boolean isTrue;
        isTrue = check(".*[a-z]+.*", password) & check(".*[A-Z]+.*", password) & check(".*[0-9]+.*", password);
        return isTrue;
    }

    private boolean check(String s, String pass) {
        Pattern p;
        Matcher m;
        p = Pattern.compile(s);
        m = p.matcher(pass);
        return m.matches();
    }
}
