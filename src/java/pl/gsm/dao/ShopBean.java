package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import pl.gsm.entity.Shop;

@ManagedBean(name = "shopBean")
@SessionScoped
public class ShopBean {

    private Shop shop = new Shop();
    private EntityManager em;

    public String addShop() {
        startEntityManager();
        shop.setId(0);
        em.merge(shop);
        stopEntityManager();
        return null;
    }

    public String deleteShop() {
        startEntityManager();
        this.shop = em.find(Shop.class, shop.getId());
        em.remove(this.shop);
        stopEntityManager();
        return null;
    }

    public String loadShop() {
        em = DBManager.getManager().createEntityManager();
        this.shop = em.find(Shop.class, shop.getId());
        em.close();
        return "/editshop.xhtml";
    }

    public String editShop() {
        startEntityManager();
        em.merge(shop);
        stopEntityManager();
        return "/addshop.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.shop = new Shop();
        em.getTransaction().commit();
        em.close();
    }

    public void shopListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("shopID");
        int id = Integer.parseInt(ids);
        this.shop.setId(id);
    }

    public List<Shop> getShopList() {
        em = DBManager.getManager().createEntityManager();
        List shopList = em.createNamedQuery("Shop.findAll").getResultList();
        em.close();
        return shopList;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

}
