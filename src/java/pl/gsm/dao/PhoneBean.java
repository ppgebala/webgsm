package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pl.gsm.bean.LoginBean;
import pl.gsm.entity.Adress;
import pl.gsm.entity.DailySales;
import pl.gsm.entity.Equipment;
import pl.gsm.entity.Phone;
import pl.gsm.entity.Seller;
import pl.gsm.entity.Tax;

@ManagedBean
@SessionScoped
public class PhoneBean {

    private Phone phone = new Phone();
    private Equipment equipment = new Equipment();
    private Seller seller = new Seller();
    private Adress adress = new Adress();
    private EntityManager em;

    @PostConstruct
    protected void init() {
        setFirstTax();
    }

    public String addPhone() {
        startEntityManager();
        equipment.setId(0);
        seller.setId(0);
        adress.setId(0);
        phone.setId(0);
        phone.setPurchaseDate(new Date());
        phone.setEquipmentId(equipment);
        phone.setSellerId(seller);
        phone.setShopId(LoginBean.getShop());
        seller.setAdressId(adress);
        em.merge(phone);
        stopEntityManager();
        return null;
    }

    public String deletePhone() {
        startEntityManager();
        this.setPhone(em.find(Phone.class, phone.getId()));
        em.remove(this.phone);
        stopEntityManager();
        return null;
    }

    public String loadPhone() {
        em = DBManager.getManager().createEntityManager();
        this.phone = em.find(Phone.class, phone.getId());
        this.equipment.setId(phone.getEquipmentId().getId());
        this.equipment = em.find(Equipment.class, equipment.getId());
        this.seller.setId(phone.getSellerId().getId());
        this.seller = em.find(Seller.class, seller.getId());
        this.adress.setId(phone.getSellerId().getAdressId().getId());
        this.adress = em.find(Adress.class, adress.getId());
        em.close();
        return "/edit/editphone.xhtml";
    }

    public String editPhone() {
        startEntityManager();
        em.merge(phone);
        stopEntityManager();
        return "/addphone.xhtml";
    }

    public String sellPhone() {
        em = DBManager.getManager().createEntityManager();
        this.phone = em.find(Phone.class, phone.getId());
        em.close();

        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();

        DailySales tempDailySales = new DailySales();
        tempDailySales.setId(0);
        tempDailySales.setEmployeeUserId(LoginBean.getUser());
        tempDailySales.setShopId(LoginBean.getShop());
        tempDailySales.setCategory("TELEFON");
        tempDailySales.setProduct(phone.getProducer() + " " + phone.getModel() + " id=" + String.valueOf(phone.getId()));
        tempDailySales.setNumber(1);
        tempDailySales.setUnitPrice(phone.getGrossSellingPrice());
        tempDailySales.setSumPrice(phone.getGrossSellingPrice());
        em.merge(tempDailySales);
        phone.setSaleDate(new Date());
        em.merge(phone);

        em.getTransaction().commit();
        em.close();

        return null;
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        em.getTransaction().commit();
        em.close();
        this.phone = new Phone();
        equipment = new Equipment();
        seller = new Seller();
        adress = new Adress();
        setFirstTax();
    }

    public void phoneListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("phoneID");
        int id = Integer.parseInt(ids);
        this.phone.setId(id);
    }

    public List<Phone> getPhoneList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Phone> query = em.createQuery("select e from Phone e where e.shopId = :shopId and e.saleDate IS NULL", Phone.class);
        query.setParameter("shopId", LoginBean.getShop());
        List phoneList = query.getResultList();
        em.close();
        return phoneList;
    }

    public List<Phone> getSellPhoneList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Phone> query = em.createQuery("select e from Phone e where e.shopId = :shopId and e.saleDate IS NOT NULL", Phone.class);
        query.setParameter("shopId", LoginBean.getShop());
        List phoneList = query.getResultList();
        em.close();
        return phoneList;
    }

    private void setFirstTax() {
        TaxBean tempTaxBean = new TaxBean();
        Tax tempTax = tempTaxBean.getTaxList().get(0);
        phone.setTaxId(new Tax(tempTax.getId(), tempTax.getRate()));
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }
}
