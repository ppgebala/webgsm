package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import pl.gsm.entity.Tax;

@ManagedBean(name = "taxBean")
@SessionScoped
public class TaxBean {

    private Tax tax = new Tax();
    private EntityManager em;

    public String addTax() {
        startEntityManager();
        getTax().setId(0);
        em.merge(getTax());
        stopEntityManager();
        return null;
    }

    public String deleteTax() {
        startEntityManager();
        this.tax = em.find(Tax.class, tax.getId());
        em.remove(this.tax);
        stopEntityManager();
        return null;
    }

    public String loadTax() {
        em = DBManager.getManager().createEntityManager();
        this.tax = em.find(Tax.class, tax.getId());
        em.close();
        return "/edittax.xhtml";
    }

    public String editTax() {
        startEntityManager();
        em.merge(tax);
        stopEntityManager();
        return "/addtax.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.tax = new Tax();
        em.getTransaction().commit();
        em.close();
    }

    public void taxListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("taxID");
        int id = Integer.parseInt(ids);
        this.tax.setId(id);
    }

    public List<Tax> getTaxList() {
        em = DBManager.getManager().createEntityManager();
        List taxList = em.createNamedQuery("Tax.findAll").getResultList();
        em.close();
        return taxList;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

}
