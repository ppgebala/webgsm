package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pl.gsm.bean.LoginBean;
import pl.gsm.entity.Goods;
import pl.gsm.entity.Product;
import pl.gsm.entity.Tax;
import pl.gsm.entity.TypeGoods;

@ManagedBean(name = "goodsBean")
@SessionScoped
public class GoodsBean {

    private Goods goods = new Goods();
    private Product product = new Product();
    private EntityManager em;

    @PostConstruct
    protected void init() {
        setFirstTax();
        setFirstTypeGoods(0);
    }

    public String addGoods() {
        startEntityManager();
        getGoods().setId(0);
        goods.setAddedDate(new Date());
        goods.setModyficationDate(new Date());
        goods.setShopId(LoginBean.getShop());
        em.merge(getGoods());
        stopEntityManager();
        return null;
    }

    public String deleteGoods() {
        startEntityManager();
        this.setGoods(em.find(Goods.class, getGoods().getId()));
        em.remove(this.getGoods());
        stopEntityManager();
        return null;
    }

    public String loadGoods() {
        em = DBManager.getManager().createEntityManager();
        this.goods = em.find(Goods.class, goods.getId());
        em.close();
        return "/edit/editgoods.xhtml";
    }

    public String editGoods() {
        startEntityManager();
        goods.setModyficationDate(new Date());
        em.merge(goods);
        stopEntityManager();
        return "/addgoods.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        em.getTransaction().commit();
        em.close();
        TypeGoods tempTypeGoods = goods.getTypeGoodsId();
        this.goods = new Goods();
        setFirstTax();
        goods.setTypeGoodsId(tempTypeGoods);
    }

    public void goodsListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("goodsID");
        int id = Integer.parseInt(ids);
        this.getGoods().setId(id);
    }

    public List<Product> getProductList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Product> query = em.createQuery("select e from Product e where e.typeGoodsId = :typeGoodsId", Product.class);
        query.setParameter("typeGoodsId", new TypeGoods(goods.getTypeGoodsId().getId()));
        List productList = query.getResultList();
        em.close();
        return productList;
    }

    public List<Goods> getGoodsList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Goods> query = em.createQuery("select e from Goods e where e.typeGoodsId = :typeGoodsId and e.shopId = :shopId", Goods.class);
        query.setParameter("typeGoodsId", new TypeGoods(goods.getTypeGoodsId().getId()));
        query.setParameter("shopId", LoginBean.getShop());
        List goodsList = query.getResultList();
        em.close();
        return goodsList;
    }

    private void setFirstTax() {
        TaxBean tempTaxBean = new TaxBean();
        Tax tempTax = tempTaxBean.getTaxList().get(0);
        goods.setTaxId(new Tax(tempTax.getId(), tempTax.getRate()));
    }

    private void setFirstTypeGoods(int id) {
        TypeGoodsBean tempTypeGoodsBean = new TypeGoodsBean();
        TypeGoods tempTypeGoods = tempTypeGoodsBean.getTypeGoodsList().get(id);
        goods.setTypeGoodsId(new TypeGoods(tempTypeGoods.getId(), tempTypeGoods.getCategoryName()));
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
