package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import pl.gsm.entity.EmployeeUserType;

@ManagedBean(name = "employeeUserTypeBean")
@SessionScoped
public class EmployeeUserTypeBean {

    private EmployeeUserType userType = new EmployeeUserType();
    private EntityManager em;

    public String addEmployeeUserType() {
        startEntityManager();
        userType.setId(0);
        em.merge(userType);
        stopEntityManager();
        return null;
    }

    public String deleteEmployeeUserType() {
        startEntityManager();
        this.userType = em.find(EmployeeUserType.class, userType.getId());
        em.remove(this.userType);
        stopEntityManager();
        return null;
    }

    public String loadEmployeeUserType() {
        em = DBManager.getManager().createEntityManager();
        this.userType = em.find(EmployeeUserType.class, userType.getId());
        em.close();
        return "/editusertype.xhtml";
    }

    public String editEmployeeUserType() {
        startEntityManager();
        em.merge(userType);
        stopEntityManager();
        return "/addtax.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.userType = new EmployeeUserType();
        em.getTransaction().commit();
        em.close();
    }

    public void employeeUserTypeListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("employeeusertypeID");
        int id = Integer.parseInt(ids);
        this.userType.setId(id);
    }

    public List<EmployeeUserType> getEmployeeUserTypeList() {
        em = DBManager.getManager().createEntityManager();
        List employeeUserTypeList = em.createNamedQuery("EmployeeUserType.findAll").getResultList();
        return employeeUserTypeList;
    }

    public EmployeeUserType getUserType() {
        return userType;
    }

    public void setUserType(EmployeeUserType userType) {
        this.userType = userType;
    }

}
