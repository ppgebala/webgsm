package pl.gsm.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DBManager {

    private static DBManager instance;
    private EntityManagerFactory emf;

    private DBManager() {
    }

    public EntityManagerFactory createEntityManagerFactory() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("webGSMPU");
        }
        return emf;
    }

    public EntityManager createEntityManager() {
        return this.createEntityManagerFactory().createEntityManager();
    }

    public void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
        }
    }

    public synchronized static DBManager getManager() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

}
