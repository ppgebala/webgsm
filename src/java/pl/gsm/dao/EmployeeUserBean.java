package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import pl.gsm.entity.EmployeeUser;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

@ManagedBean(name = "employeeUserBean")
@SessionScoped
public class EmployeeUserBean {

    private EmployeeUser user = new EmployeeUser();
    private EntityManager em;

    public static boolean checkLoginAndPassword(String user, String password) {
        EntityManager em = DBManager.getManager().createEntityManager();
        try {
            TypedQuery<EmployeeUser> query = em.createQuery("select e from EmployeeUser e where e.userLogin = :userLogin and e.userPassword = :userPassword", EmployeeUser.class);
            query.setParameter("userLogin", user);
            query.setParameter("userPassword", password);

            List<EmployeeUser> matchingUser = query.getResultList();
            return !matchingUser.isEmpty();
        } catch (Exception ex) {
            System.out.println("Error login -->" + ex.getMessage());
            return false;
        } finally {
            em.close();
        }
    }

    public String addEmployeeUser() {
        startEntityManager();
        user.setId(0);
        em.merge(user);
        stopEntityManager();
        return null;
    }

    public String deleteEmployeeUser() {
        startEntityManager();
        this.user = em.find(EmployeeUser.class, user.getId());
        em.remove(this.user);
        stopEntityManager();
        return null;
    }

    public String loadEmployeeUser() {
        em = DBManager.getManager().createEntityManager();
        this.user = em.find(EmployeeUser.class, user.getId());
        em.close();
        return "/edit/editemployeeuser.xhtml";
    }

    public String editEmployeeUser() {
        startEntityManager();
        em.merge(user);
        stopEntityManager();
        return "/addtax.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.user = new EmployeeUser();
        em.getTransaction().commit();
        em.close();
    }

    public void employeeListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("employeeuserID");
        int id = Integer.parseInt(ids);
        this.user.setId(id);
    }

    public List<EmployeeUser> getEmployeeUserList() {
        em = DBManager.getManager().createEntityManager();
        List employeeUserList = em.createNamedQuery("EmployeeUser.findAll").getResultList();
        em.close();
        return employeeUserList;
    }

    public EmployeeUser getUser() {
        return user;
    }

    public void setUser(EmployeeUser user) {
        this.user = user;
    }

}
