package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pl.gsm.bean.LoginBean;
import pl.gsm.entity.DailySales;
import pl.gsm.entity.Service;

@ManagedBean(name = "serviceBean")
@SessionScoped
public class ServiceBean {

    private Service service = new Service();
    private EntityManager em;

    public String addService() {
        startEntityManager();
        service.setId(0);
        service.setShopId(LoginBean.getShop());
        em.merge(getService());
        stopEntityManager();
        return null;
    }

    public String deleteService() {
        startEntityManager();
        this.setService(em.find(Service.class, getService().getId()));
        em.remove(this.getService());
        stopEntityManager();
        return null;
    }

    public String loadService() {
        em = DBManager.getManager().createEntityManager();
        this.setService(em.find(Service.class, getService().getId()));
        em.close();
        return "/edit/editservice.xhtml";
    }

    public String editService() {
        startEntityManager();
        em.merge(getService());
        stopEntityManager();
        return "/addservice.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.setService(new Service());
        em.getTransaction().commit();
        em.close();
    }

    public String sellService() {
        em = DBManager.getManager().createEntityManager();
        this.service = em.find(Service.class, service.getId());
        em.close();

        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();

        DailySales tempDailySales = new DailySales();
        tempDailySales.setId(0);
        tempDailySales.setEmployeeUserId(LoginBean.getUser());
        tempDailySales.setShopId(LoginBean.getShop());
        tempDailySales.setCategory("SERWIS");
        tempDailySales.setProduct(service.getPhoneService());
        tempDailySales.setNumber(1);
        tempDailySales.setUnitPrice(service.getExpense());
        tempDailySales.setSumPrice(service.getExpense());
        em.merge(tempDailySales);
        service.setDateOfReceipt(new Date());
        em.merge(service);
        em.getTransaction().commit();
        em.close();

        return null;
    }

    public void serviceListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("serviceID");
        int id = Integer.parseInt(ids);
        this.getService().setId(id);
    }

    public List<Service> getServiceList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Service> query = em.createQuery("select e from Service e where e.shopId = :shopId and e.dateOfReceipt IS NULL", Service.class);
        query.setParameter("shopId", LoginBean.getShop());
        List serviceList = query.getResultList();
        em.close();
        return serviceList;
    }

    public List<Service> getSellServiceList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Service> query = em.createQuery("select e from Service e where e.shopId = :shopId and e.dateOfReceipt IS NOT NULL", Service.class);
        query.setParameter("shopId", LoginBean.getShop());
        List serviceList = query.getResultList();
        em.close();
        return serviceList;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

}
