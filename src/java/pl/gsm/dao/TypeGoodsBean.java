package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import pl.gsm.entity.TypeGoods;

@ManagedBean(name = "typeGoodsBean")
@SessionScoped
public class TypeGoodsBean {

    private TypeGoods typeGoods = new TypeGoods();
    private EntityManager em;

    public String addTypeGoods() {
        startEntityManager();
        getTypeGoods().setId(0);
        em.merge(getTypeGoods());
        stopEntityManager();
        return null;
    }

    public String deleteTypeGoods() {
        startEntityManager();
        this.typeGoods = em.find(TypeGoods.class, typeGoods.getId());
        em.remove(this.typeGoods);
        stopEntityManager();
        return null;
    }

    public String loadTypeGoods() {
        em = DBManager.getManager().createEntityManager();
        this.typeGoods = em.find(TypeGoods.class, typeGoods.getId());
        em.close();
        return "/edittype.xhtml";
    }

    public String editTypeGoods() {
        startEntityManager();
        em.merge(typeGoods);
        stopEntityManager();
        return "/addtax.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        this.typeGoods = new TypeGoods();
        em.getTransaction().commit();
        em.close();
    }

    public void typeGoodsListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("typeGoodsID");
        int id = Integer.parseInt(ids);
        this.typeGoods.setId(id);
    }

    public List<TypeGoods> getTypeGoodsList() {
        em = DBManager.getManager().createEntityManager();
        List typeGoodsList = em.createNamedQuery("TypeGoods.findAll").getResultList();
        typeGoodsList.add(0, new TypeGoods(-1, "Wybierz"));
        em.close();
        return typeGoodsList;
    }

    public TypeGoods getTypeGoods() {
        return typeGoods;
    }

    public void setTypeGoods(TypeGoods typeGoods) {
        this.typeGoods = typeGoods;
    }

}
