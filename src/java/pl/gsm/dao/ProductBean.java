package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pl.gsm.entity.Product;
import pl.gsm.entity.TypeGoods;

@ManagedBean(name = "productBean")
@SessionScoped
public class ProductBean {

    private Product product = new Product();
    private EntityManager em;

    @PostConstruct
    protected void init() {
        setFirstTypeGoods(0);
    }

    public String addProduct() {
        startEntityManager();
        product.setId(0);
        em.merge(product);
        stopEntityManager();
        return null;
    }

    public String deleteProduct() {
        startEntityManager();
        this.product = em.find(Product.class, product.getId());
        em.remove(this.product);
        stopEntityManager();
        return null;
    }

    public String loadProduct() {
        em = DBManager.getManager().createEntityManager();
        this.product = em.find(Product.class, product.getId());
        em.close();
        return "/edit/editproduct.xhtml";
    }

    public String editProduct() {
        startEntityManager();
        em.merge(product);
        stopEntityManager();
        return "/addproduct.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        em.getTransaction().commit();
        em.close();
        TypeGoods tempTypeGoods = product.getTypeGoodsId();
        this.product = new Product();
        product.setTypeGoodsId(tempTypeGoods);
    }

    public void productListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productID");
        int id = Integer.parseInt(ids);
        this.product.setId(id);
    }

    public List<Product> getProductList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Product> query = em.createQuery("select e from Product e where e.typeGoodsId = :typeGoodsId ORDER BY e.name ASC", Product.class);
        query.setParameter("typeGoodsId", new TypeGoods(product.getTypeGoodsId().getId()));
        List productList = query.getResultList();
        em.close();
        return productList;
    }

    private void setFirstTypeGoods(int id) {
        TypeGoodsBean tempTypeGoodsBean = new TypeGoodsBean();
        TypeGoods tempTypeGoods = tempTypeGoodsBean.getTypeGoodsList().get(id);
        product.setTypeGoodsId(new TypeGoods(tempTypeGoods.getId(), tempTypeGoods.getCategoryName()));
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
