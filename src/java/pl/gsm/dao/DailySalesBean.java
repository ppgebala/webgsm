package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.primefaces.event.SelectEvent;
import pl.gsm.bean.LoginBean;
import pl.gsm.entity.DailySales;
import pl.gsm.entity.DailySummaryReport;
import pl.gsm.entity.Goods;
import pl.gsm.entity.TypeGoods;

@ManagedBean(name = "dailySalesBean")
@SessionScoped
public class DailySalesBean implements Serializable {

    private DailySales dailySales = new DailySales();
    private Goods goods = new Goods();
    EntityManager em;
    private Integer num = 0;

    private HtmlInputText otherCategory = new HtmlInputText();
    private HtmlInputText otherProduct = new HtmlInputText();
    private HtmlSelectOneMenu typeGoodsSOM = new HtmlSelectOneMenu();
    private HtmlSelectOneMenu productSOM = new HtmlSelectOneMenu();

    private BigDecimal cashPositive;
    private BigDecimal cashNegative;
    private BigDecimal dailyReport;
    private BigDecimal differenceReport;
    private Date dateDown = new Date();
    private Date dateUp = new Date();
    private Date today = new Date();
    private boolean close = false;

    private DailySummaryReportBean dsrb;

    @PostConstruct
    protected void init() {
        dailySales.setNumber(1);
        dailySales.setUnitPrice(new BigDecimal(BigInteger.ZERO));
    }

    public String addDailySales() {
        startEntityManager();
        getDailySales().setId(0);
        dailySales.setShopId(LoginBean.getShop());
        dailySales.setEmployeeUserId(LoginBean.getUser());
        em.merge(getDailySales());
        stopEntityManager();
        return null;
    }

    public String deleteDailySales() {
        startEntityManager();
        this.dailySales = em.find(DailySales.class, dailySales.getId());
        em.remove(this.dailySales);
        stopEntityManager();
        return null;
    }

    public String loadDailySales() {
        em = DBManager.getManager().createEntityManager();
        this.dailySales = em.find(DailySales.class, dailySales.getId());
        em.close();
        return "/editdailysales.xhtml";
    }

    public String editDailySales() {
        startEntityManager();
        em.merge(dailySales);
        stopEntityManager();
        return "/adddailysales.xhtml";
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        em.getTransaction().commit();
        em.close();
        this.dailySales = new DailySales();
        this.goods = new Goods();
        dailySales.setNumber(1);
        dailySales.setUnitPrice(new BigDecimal(BigInteger.ZERO));
        num = null;
    }

    public void setClosed() {
        close = checkCloseDay();
        if (close == true) {
            otherCategory.setDisabled(true);
            otherProduct.setDisabled(true);
            typeGoodsSOM.setDisabled(true);
            productSOM.setDisabled(true);
        } else {
            typeGoodsSOM.setDisabled(false);
            productSOM.setDisabled(false);
        }
    }

    public boolean checkCloseDay() {
        em = DBManager.getManager().createEntityManager();
        Calendar c = Calendar.getInstance(),
                c1 = Calendar.getInstance(),
                c2 = Calendar.getInstance();
        c.setTime(new Date());
        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        c.setTime(new Date());
        c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) + 1);
        try {
            TypedQuery<DailySummaryReport> query = em.createQuery("select e from DailySummaryReport e where e.summaryDate > :dateDown and e.summaryDate < :dateUp", DailySummaryReport.class);

            query.setParameter("dateDown", c1, TemporalType.DATE);
            query.setParameter("dateUp", c2, TemporalType.DATE);

            List<DailySummaryReport> matchingSummary = query.getResultList();
            return !matchingSummary.isEmpty();
        } catch (Exception ex) {
            return false;
        } finally {
            em.close();
        }
    }

    public List<TypeGoods> getCategoryList() {
        em = DBManager.getManager().createEntityManager();
        List categoryList = em.createNamedQuery("TypeGoods.findAll").getResultList();
        categoryList.add(0, new TypeGoods(-1, "Wybierz"));
        em.close();
        return categoryList;
    }

    public List<DailySales> getDailySalesList() {
        List dailySalesList;
        Calendar c = Calendar.getInstance(),
                c1 = Calendar.getInstance(),
                c2 = Calendar.getInstance();
        c.setTime(dateDown);
        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        c.setTime(dateUp);
        c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) + 1);

        em = DBManager.getManager().createEntityManager();
        TypedQuery<DailySales> query = em.createQuery("select e from DailySales e WHERE e.sellingDate > :dateDown and e.sellingDate < :dateUp and e.shopId = :shopId", DailySales.class);

        query.setParameter("dateDown", c1, TemporalType.DATE);
        query.setParameter("dateUp", c2, TemporalType.DATE);
        query.setParameter("shopId", LoginBean.getShop());

        dailySalesList = query.getResultList();
        em.close();
        return dailySalesList;
    }

    public List<Goods> getGoodsList() {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Goods> query = em.createQuery("select e from Goods e where e.typeGoodsId.categoryName = :category and e.shopId = :shopId and e.number > 0", Goods.class);
        query.setParameter("category", dailySales.getCategory());
        query.setParameter("shopId", LoginBean.getShop());
        List goodsList = query.getResultList();
        em.close();
        return goodsList;
    }

    public String add() {
        if (otherCategory.isDisabled()) {
            substractGoods();
        }
        addDailySales();
        return "";
    }

    public String substractGoods() {
        em = DBManager.getManager().createEntityManager();
        this.goods = em.find(Goods.class, goods.getId());
        em.close();
        startEntityManager();
        goods.setNumber(goods.getNumber() - dailySales.getNumber());
        em.merge(goods);
        em.getTransaction().commit();
        em.close();
        this.goods = new Goods();
        return "";
    }

    public String closeAndSet() {
        closeDay();
        setClosed();
        return null;
    }

    public void closeDay() {
        dsrb = new DailySummaryReportBean();
        dsrb.getDailySummaryReport().setMoneyEarned(cashPositive);
        dsrb.getDailySummaryReport().setMoneySpent(cashNegative);
        dsrb.getDailySummaryReport().setMoneySum(cashPositive.add(cashNegative));
        dsrb.getDailySummaryReport().setMoneyReport(dailyReport);
        dsrb.getDailySummaryReport().setTotalStateCash(dailyReport);
        dsrb.addDailySummaryReport();
    }

    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

    public String change() {
        if (otherCategory.isDisabled()) {
            otherCategory.setDisabled(false);
            otherProduct.setDisabled(false);
            typeGoodsSOM.setDisabled(true);
            productSOM.setDisabled(true);
            dailySales.setCategory("INNY");
        } else {
            otherCategory.setDisabled(true);
            otherProduct.setDisabled(true);
            typeGoodsSOM.setDisabled(false);
            productSOM.setDisabled(false);
        }
        return "";
    }

    public void dailySalesGoodsListener(ActionEvent ae) {
        em = DBManager.getManager().createEntityManager();
        TypedQuery<Goods> query = em.createQuery("select e from Goods e where e.id = :id", Goods.class);
        query.setParameter("id", goods.getId());
        Goods tempGoods = query.getSingleResult();
        em.close();
        num = tempGoods.getNumber();
        dailySales.setProduct(tempGoods.getName());
        dailySales.setUnitPrice(tempGoods.getGrossSellingPrice());
    }

    public void dailySalesListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dailysalesID");
        int id = Integer.parseInt(ids);
        this.dailySales.setId(id);
    }

    public void dailySalesNumberListener(ActionEvent ae) {
        dailySales.setSumPrice(dailySales.getUnitPrice().multiply(
                new BigDecimal(dailySales.getNumber())));
    }

    public void dailySalesReportListener(ActionEvent ae) {
        this.differenceReport = cashPositive.add(cashNegative).subtract(dailyReport);
    }

    public void summaryListener(ActionEvent ae) {
        List<DailySales> dailySalesList;
        cashPositive = new BigDecimal(BigInteger.ZERO);
        cashNegative = new BigDecimal(BigInteger.ZERO);
        dailySalesList = getDailySalesList();
        for (DailySales x : dailySalesList) {
            if ((x.getSumPrice().compareTo(new BigDecimal(BigInteger.ZERO))) == -1) {
                cashNegative = cashNegative.add(x.getSumPrice());
            } else {
                cashPositive = cashPositive.add(x.getSumPrice());
            }
        }
    }

    public DailySales getDailySales() {
        return dailySales;
    }

    public void setDailySales(DailySales dailySales) {
        this.dailySales = dailySales;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public HtmlInputText getOtherCategory() {
        return otherCategory;
    }

    public void setOtherCategory(HtmlInputText otherCategory) {
        this.otherCategory = otherCategory;
    }

    public HtmlSelectOneMenu getTypeGoodsSOM() {
        return null;
    }

    public void setTypeGoodsSOM(HtmlSelectOneMenu typeGoodsSOM) {
        this.typeGoodsSOM = typeGoodsSOM;
    }

    public HtmlSelectOneMenu getProductSOM() {
        return null;
    }

    public void setProductSOM(HtmlSelectOneMenu productSOM) {
        this.productSOM = productSOM;
    }

    public HtmlInputText getOtherProduct() {
        return otherProduct;
    }

    public void setOtherProduct(HtmlInputText otherProduct) {
        this.otherProduct = otherProduct;
    }

    public BigDecimal getCashPositive() {
        return cashPositive;
    }

    public void setCashPositive(BigDecimal cashPositive) {
        this.cashPositive = cashPositive;
    }

    public BigDecimal getCashNegative() {
        return cashNegative;
    }

    public void setCashNegative(BigDecimal cashNegative) {
        this.cashNegative = cashNegative;
    }

    public Date getDateDown() {
        return dateDown;
    }

    public void setDateDown(Date dateDown) {
        this.dateDown = dateDown;
    }

    public Date getDateUp() {
        return dateUp;
    }

    public void setDateUp(Date dateUp) {
        this.dateUp = dateUp;
    }

    public BigDecimal getDailyReport() {
        return dailyReport;
    }

    public void setDailyReport(BigDecimal dailyReport) {
        this.dailyReport = dailyReport;
    }

    public BigDecimal getDifferenceReport() {
        return differenceReport;
    }

    public void setDifferenceReport(BigDecimal differenceReport) {
        this.differenceReport = differenceReport;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean aClose) {
        close = aClose;
    }
}
