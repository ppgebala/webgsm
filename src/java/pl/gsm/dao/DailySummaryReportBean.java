package pl.gsm.dao;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import pl.gsm.entity.DailySummaryReport;

@ManagedBean(name = "dailySummaryReportBean")
@SessionScoped
public class DailySummaryReportBean {

    private DailySummaryReport dailySummaryReport = new DailySummaryReport();
    EntityManager em;

    public String addDailySummaryReport() {
        startEntityManager();
        getDailySummaryReport().setId(0);
        em.merge(getDailySummaryReport());
        stopEntityManager();
        DailySalesBean dsb = new DailySalesBean();
        dsb.setClosed();
        return null;
    }

    public String deleteDailySummaryReport() {
        startEntityManager();
        setDailySummaryReport(em.find(DailySummaryReport.class, getDailySummaryReport().getId()));
        em.remove(this.getDailySummaryReport());
        stopEntityManager();
        return null;
    }

    public String loadDailySummaryReport(){
        em = DBManager.getManager().createEntityManager();
        setDailySummaryReport(em.find(DailySummaryReport.class, getDailySummaryReport().getId()));
        em.close();
        return "/dailySummaryReporttax.xhtml";
    }
    
    public String editDailySummaryReport(){
        startEntityManager();
        em.merge(getDailySummaryReport());
        stopEntityManager();
        return "/dailySummaryReporttax.xhtml";
    }

    public void dailySummaryReportListener(ActionEvent ae) {
        String ids = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("dailySummaryReportID");
        int id = Integer.parseInt(ids);
        this.getDailySummaryReport().setId(id);
    }

    public List<DailySummaryReport> getDailySummaryReportList() {
        em = DBManager.getManager().createEntityManager();
        List dailySummaryReportList = em.createNamedQuery("DailySummaryReport.findAll").getResultList();
        em.close();
        return dailySummaryReportList;
    }

    private void startEntityManager() {
        em = DBManager.getManager().createEntityManager();
        em.getTransaction().begin();
    }

    private void stopEntityManager() {
        em.getTransaction().commit();
        em.close();
        this.setDailySummaryReport(new DailySummaryReport());
    }

    public DailySummaryReport getDailySummaryReport() {
        return dailySummaryReport;
    }

    public void setDailySummaryReport(DailySummaryReport dailySummaryReport) {
        this.dailySummaryReport = dailySummaryReport;
    }

}
