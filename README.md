## Założenia aplikacji webGSM ##
Aplikacja została stworzona na potrzeby firmy handlowo-usługowej zajmującej się
skupem i sprzedażą telefonów komórkowych, handlem akcesoriami GSM oraz
prowadzeniem napraw serwisowych urządzeń. Na firmę składa się kilka punktów
rozmieszczonych w różnych miastach. Głównym założeniem jest zapewnienie pewnego
stopnia kontroli dla właściciela przedsiębiorstwa.
Tym sposobem aplikacja pozwala na: zarządzanie stanem magazynu (dodawanie oraz
podgląd listy towarów aktualnie dostępnych), prowadzenie dziennika sprzedaży, ustalanie
planu napraw serwisowych, generowanie faktur dla klienta na podstawie sprzedanych
przedmiotów lub wykonanych usług oraz przeglądanie czasowych statystyk sprzedaży.
System jest przystosowany do konkretnej firmy, gdzie ilość klientów odwiedzających
poszczególne punkty jest stosunkowo niewielka. Wymaganie to jest podyktowane tym, że
system nie wspiera obsługi czytników kodów kreskowych, więc wszystkie transakcje są
wprowadzane przez pracowników ręcznie.
Program jest dostępny dla właściciela oraz sprzedawców z poszczególnych punktów za
pośrednictwem przeglądarki internetowej. W celu rozpoczęcia pracy z aplikacją wymagane
jest zalogowanie do systemu.
Aplikacja została napisana w języku Java na platformie Jave EE i wykorzystuje
frameworki JSF, Hibernate oraz bazę danych PostgreSQL.

## Wymagania funkcjonalne ##

Do wymagań funkcjonalnych należą:
### Zarządzanie stanem magazynu towarów podstawowych: ###
* dodawanie, usuwanie, edycja i przeglądanie pozycji magazynowych,
* dodawanie, usuwanie, edycja i przeglądanie kategorii produktów,
* dodawanie informacji o kategorii, nazwie oraz cenie towarów.
### Zarządzanie stanem magazynu telefonów komórkowych: ###
* zapewnienie oddzielnej listy towarów dla telefonów komórkowych,
* dodawanie, usuwanie, edycja i przeglądanie posiadanych telefonów,
* dodawanie informacji podstawowych takich jak producent, model, numer IMEI
telefonu, informacje o blokadzie simlock i gwarancji,
* dodawanie informacji o akcesoriach, które dołączane są do telefonu,
* dodawanie informacji o sprzedawcy, od którego telefon został zakupiony.
### Zarządzanie stanem serwisu: ###
* dodawanie, usuwanie, edycja i przeglądanie pozycji serwisowych,
* dodawanie informacji o przyjętym sprzęcie oraz opisie usterki,
* możliwość zmiany stanu pozycji serwisowej z oczekującej na wykonaną.
### Prowadzenie dziennika sprzedaży: ###
* zapisywanie sprzedanych pozycji z magazynu towarów podstawowych,
* zapisywanie sprzedanych pozycji z magazynu telefonów,
* zapisywanie wykonanych napraw serwisowych,
* zapisywanie innych pozycji, które nie obejmują magazynu (np. doładowania
telefonów),
* zapisywanie pozycji z wartością ujemną (np. płatności wykonane kartą płatniczą,
koszty przyjęcia paczek i inne wydatki danego punktu),
* dodawanie informacji o produkcie, dacie sprzedaży, identyfikatorze sprzedawcy,
ilości sprzedanych towarów, cenie jednostkowej oraz sumie wartości,
* wyświetlanie ilości pozycji pozostałych w magazynie,
* możliwość wyświetlania historii sprzedaży z danego zakresu czasowego,
* wyświetlanie dodatniej kwoty zebranej danego dnia,
* wyświetlanie ujemnej kwoty zebranej danego dnia,
* obliczanie całkowitego utargu z danego dnia (różnica kwoty dodatniej i ujemnej),
* możliwość wprowadzania utargu ręcznie (utarg obliczony na podstawie raportu
kasy fiskalnej),
* możliwość zamykania danego dnia, równoznaczna z blokadą dodawania nowych
produktów oraz zapisem podsumowania dnia do bazy danych.
### Obsługa faktur VAT: ###
* możliwość wyboru pozycji, na które ma zostać wystawiona faktura VAT ze zbioru
towarów/usług zapisanych w dzienniku sprzedaży,
* dodawanie informacji o firmie, sprzedawcy, nabywcy, dacie sprzedaży
i wystawienia faktury, miejscu dodania faktury, oraz zapłaconej kwocie,
* obliczanie sumy należności widniejącej na fakturze,
* generowanie gotowego szablonu oraz zapis faktury VAT do pliku PDF.
### Obsługa kilku oddziałów firmy: ###
* możliwość przypisania stanu magazynów oraz dziennika sprzedaży do
poszczególnych punktów.
### Obsługa wielu użytkowników: ###
* rejestracja i logowanie użytkowników w serwisie,
* nadawanie praw dostępu.
### Obsługa panelu administracyjnego: ###
* dodawanie, usuwanie, edycja i wyświetlanie informacji o: stawkach VAT,
pracownikach oraz oddziałach firmy.

## Interfejs graficzny ##

![2.png](https://bitbucket.org/repo/k69rb6/images/3897964943-2.png)

![3.png](https://bitbucket.org/repo/k69rb6/images/301108271-3.png)

![5.png](https://bitbucket.org/repo/k69rb6/images/2033384978-5.png)